Source: flask-seeder
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org> 
Uploaders: Joao Pedro Moura <joaopedrohm17@gmail.com>
Build-Depends: debhelper-compat (= 12), 
               dh-python, 
               python3-setuptools, 
               python3-all,
               python3-flask,
               python3-sphinx <!nodoc>,
Standards-Version: 4.6.0
Homepage: https://pypi.org/project/Flask-Seeder/
Vcs-Browser: https://salsa.debian.org/python-team/flask-seeder
Vcs-Git: https://salsa.debian.org/python-team/flask-seeder.git
Testsuite: autopkgtest-pkg-python

Package: python3-flask-seeder
Architecture: all
Depends: ${python3:Depends}, 
         ${misc:Depends}
Suggests: python-flask-seeder-doc
Description:  Flask extension for seeding database.
 Flask-Seeder is a Flask extension to help with seeding database with initial data, 
 for example when deploying an application for the first time.

 This extensions primary focus is to help populating data once, for example in a 
 demo application where the database might get wiped over and over but you still 
 want users to have some basic data to play around with.
 .
 This package installs the library for Python 3.

Package: python-flask-seeder-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends}, 
         ${misc:Depends}
Description: Flask extension for seeding database. (common documentation)
 Flask-Seeder is a Flask extension to help with seeding database with initial data, 
 for example when deploying an application for the first time.

 This extensions primary focus is to help populating data once, for example in a 
 demo application where the database might get wiped over and over but you still 
 want users to have some basic data to play around with.
 .
 This is the common documentation package.
